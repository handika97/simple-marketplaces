const express = require("express");
const kycRouter = express.Router();
const Controller = require("../controllers/kyc");

kycRouter.post("/register", Controller.create).post("/login", Controller.login);

module.exports = kycRouter;
