const express = require("express");
const productRouter = express.Router();
const Controller = require("../controllers/product");

productRouter
  .post("/create", Controller.create)
  .get("/may/:id", Controller.getMyProduct)
  .get("/:id_category/:id_user", Controller.getProduct);

module.exports = productRouter;
