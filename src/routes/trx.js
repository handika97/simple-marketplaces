const express = require("express");
const trxRouter = express.Router();
const Controller = require("../controllers/trx");

trxRouter
  .post("/buy", Controller.buy)
  .get("/buyyer/:id", Controller.buyyer)
  .get("/seller/:id", Controller.seller);

module.exports = trxRouter;
