const express = require("express");
const kyc = require("./kyc");
const category = require("./category");
const trx = require("./trx");
const product = require("./product");
const Router = express.Router();
const Controller = require("../controllers/kyc");

Router.use("/kyc", kyc)
  .use("/product", product)
  .use("/trx", trx)
  .use("/category", category);

module.exports = Router;
