const express = require("express");
const categoryRouter = express.Router();
const Controller = require("../controllers/category");

categoryRouter
  .post("/create", Controller.create)
  .get("/", Controller.getCategory);

module.exports = categoryRouter;
