const kyc = require("../models/kyc");
const { response } = require("../helpers/response");

class KYCController {
  static create(req, res) {
    console.log(
      req.body.name,
      req.body.phone,
      req.body.email,
      req.body.password
    );
    if (
      req.body.name &&
      req.body.phone &&
      req.body.email &&
      req.body.password
    ) {
      kyc
        .create(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 200, data[0], "Success");
          } else {
            response(res, 400, null, "Error", "Sudah Adsen");
          }
        })
        .catch((err) => {
          console.log(err);
          response(res, 400, null, "Error", err);
        });
    } else {
      console.log("error");
      response(
        res,
        400,
        null,
        "Error",
        "Nama, Email, Password dan Nomer Telepon tidak boleh kosong"
      );
    }
  }
  static login(req, res) {
    if (req.body.email && req.body.password) {
      kyc
        .login(req.body)
        .then((data) => {
          console.log(data);
          if (data.length === 1) {
            response(res, 200, data[0], "Success");
          } else {
            response(res, 400, null, "Error", "Email Atau Password Salah");
          }
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      console.log("error");
      response(
        res,
        400,
        null,
        "Error",
        "Email dan Password dan tidak boleh kosong"
      );
    }
  }
}

module.exports = KYCController;
