const category = require("../models/category");
const { response } = require("../helpers/response");

class CategoryController {
  static create(req, res) {
    if (req.body.name) {
      category
        .create(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 200, null, "Success");
          } else {
            response(res, 400, null, "Error", "Sudah Adsen");
          }
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      console.log("error");
      response(res, 400, null, "Error", "Nama Category tidak boleh kosong");
    }
  }
  static getCategory(req, res) {
    category
      .getCategory()
      .then((data) => {
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
}

module.exports = CategoryController;
