const product = require("../models/product");
const { response } = require("../helpers/response");

class ProductController {
  static create(req, res) {
    if (
      req.body.id_user &&
      req.body.name &&
      req.body.price &&
      req.body.category
    ) {
      product
        .create(req.body)
        .then((data) => {
          if (data.affectedRows === 1) {
            response(res, 200, null, "Success");
          } else {
            response(res, 400, null, "Error", "Sudah Adsen");
          }
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      console.log("error");
      response(
        res,
        400,
        null,
        "Error",
        "Nama, Price dan Category tidak boleh kosong"
      );
    }
  }
  static getProduct(req, res) {
    if (req.params.id_category === "all") {
      product
        .getProductAll(req.params)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    } else {
      product
        .getProduct(req.params.id_category)
        .then((data) => {
          response(res, 200, data, "Success");
        })
        .catch((err) => {
          response(res, 400, null, "Error", err);
        });
    }
  }
  static getMyProduct(req, res) {
    product
      .getMyProduct(req.params.id)
      .then((data) => {
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
}

module.exports = ProductController;
