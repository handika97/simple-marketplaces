const trx = require("../models/trx");
const { response } = require("../helpers/response");

class trxController {
  static buy(req, res) {
    trx
      .buy(req.body)
      .then((data) => {
        if (data.affectedRows === 1) {
          response(res, 200, data[0], "Success");
        } else {
          response(res, 400, null, "Error", "Gagal Membuat Pesanan");
        }
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
  static seller(req, res) {
    trx
      .seller(req.params)
      .then((data) => {
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        console.log(err);
        response(res, 400, null, "Error", err);
      });
  }
  static buyyer(req, res) {
    trx
      .buyyer(req.params)
      .then((data) => {
        response(res, 200, data, "Success");
      })
      .catch((err) => {
        response(res, 400, null, "Error", err);
      });
  }
}

module.exports = trxController;
