module.exports = {
  response: (res, status_code, result, status, err) => {
    let resultPrint = {};
    resultPrint.status = status;
    resultPrint.status_code = status_code;
    resultPrint.data = result;
    resultPrint.error_message = err || null;
    return res.status(resultPrint.status_code).json(resultPrint);
  },
};

