const db = require("../config/db");

module.exports = {
  create: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO category(name) VALUES (?)`;
      let values = [params.name.toLowerCase()];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getCategory: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM category`;
      let values = [];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
};
