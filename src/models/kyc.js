const db = require("../config/db");

module.exports = {
  create: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO kyc(name, phone, email, password) VALUES (?,?,?,?)`;
      let values = [
        params.name.toLowerCase(),
        params.phone,
        params.email.toLowerCase(),
        params.password,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  login: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT * FROM kyc WHERE email=? AND password=? LIMIT 1`;
      let values = [params.email.toLowerCase(), params.password];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
};
