const db = require("../config/db");

module.exports = {
  buy: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO history(seller, buyyer, metode_pay, item,qty, total_price) VALUES (?,?,?,?,?,?)`;
      let values = [
        params.seller,
        params.buyyer,
        params.metode_pay,
        params.item,
        params.qty,
        params.total_price,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          console.log(err);
          reject(err.sqlMessage);
        }
      });
    });
  },
  seller: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.id, a.seller, a.buyyer, a.item , a.qty, a.total_price, a.metode_pay, b.name, b.price from history a inner join product b on a.item=b.id WHERE seller=?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          console.log(err);
          reject(err.sqlMessage);
        }
      });
    });
  },
  buyyer: (params) => {
    return new Promise((resolve, reject) => {
      console.log(params);
      let queries = `SELECT a.id, a.seller, a.buyyer, a.item , a.qty, a.total_price, a.metode_pay, b.name, b.price from history a inner join product b on a.item=b.id WHERE buyyer=?`;
      let values = [params.id];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          console.log(err);
          reject(err.sqlMessage);
        }
      });
    });
  },
};
