const db = require("../config/db");

module.exports = {
  create: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `INSERT INTO product(id_user, name,price, category) VALUES (?,?,?,?)`;
      let values = [
        params.id_user,
        params.name.toLowerCase(),
        params.price,
        params.category,
      ];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getProductAll: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT c.id as seller_id, c.name as seller_name, c.phone as seller_phone,c.email as seller_email,a.id as id_product, a.name as product_name, a.price as price, a.category as id_category, b.name as category_name FROM product a INNER JOIN category b ON a.category=b.id INNER JOIN kyc c ON a.id_user=c.id where id_user!=?`;
      let values = [params.id_user];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT c.id as seller_id, c.name as seller_name, c.phone as seller_phone,c.email as seller_email,a.id as id_product, a.name as product_name, a.price as price, a.category as id_category, b.name as category_name FROM product a INNER JOIN category b ON a.category=b.id INNER JOIN kyc c ON a.id_user=c.id WHERE category=? and id_user!=?`;
      let values = [params.id_category, params.id_user];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
  getMyProduct: (params) => {
    return new Promise((resolve, reject) => {
      let queries = `SELECT a.id,a.name, a.price, a.category as id_category, b.name as category_name FROM product a INNER JOIN category b ON a.category=b.id WHERE a.id_user=?`;
      let values = [params];
      db.query(queries, values, (err, result) => {
        if (!err) {
          resolve(result);
        } else {
          reject(err.sqlMessage);
        }
      });
    });
  },
};
