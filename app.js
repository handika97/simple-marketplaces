const express = require("express");
// const memont = require("moment");
var moment = require("moment-timezone");
require("dotenv").config();
const app = express();
const port = process.env.SERVER_PORT || 5000;
const cors = require("cors");
const routes = require("./src/routes");
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(routes);
app.listen(port, () => {
  console.log(`run on ${port}`);
});
